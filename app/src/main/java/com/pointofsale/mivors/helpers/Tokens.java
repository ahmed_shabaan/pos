package com.pointofsale.mivors.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TypefaceSpan;
import android.util.Base64;
import android.util.TypedValue;
import android.widget.ImageView;

import com.pointofsale.mivors.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Locale;


/**
 * Created by Yehia Fathi on 6/16/2016.
 */
public class Tokens {
    public final static String ip = "Replace URL";
    public final static String image_url = "http://dealandcode.com/blabla/blabla/public/images/users/";
    public final static String image_url_car = "http://dealandcode.com/blabla/blabla/public/images/cars/";
    public final static int per_page = 20;
    public final static int supplier_search = 1;
    public static final int SEARCH_CODE =1006 ;
    public static final int EDIT_PROFILE_SUCCESS = 1001;
    public static int current_order_id =0;
    public final static int item_search = 2;
    public static int search_place = Tokens.supplier_search;

    public static final String LOGO = "logo";
    public static final String COUNTRY = "country";
    public static final String NAME = "name";
    public static final String ID = "id";
    public static final String EMAIL = "email";
    public static final String PHONE = "phone";
    public static final String LINK = "link";
    public static final String SUPPLIER_ID = "SUPPLIER_ID";
    public static  long user_id = 84;
    public static  final  int REQUEST_CAMERA= 1000;
    public static final  int SELECT_FILE = 1001;
    public static int requestLogin = 1002;
    public static int requestRegister=1003;
    public static int requestchangeImage=1004;
    public static int reqest_edit_profile=1005;
    public static String image_base_car;

    public static final int  policy = 1;
    public static final int  permits = 2;
    public static final int  conditions = 3;
    public static final int  help = 4;

    public enum alert {
        login(1), VIDEO(2), AUDIO_AND_VIDEO(3);
        private final int value;

        private alert(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public enum USER_TYPES {
        patient("patient"), ambulance("ambulance");
        private final String value;

        private USER_TYPES(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }



    public static int dpToPx(int dp, Context c) {
        Resources r = c.getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }




    public static void send_email(Context c, String email) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", email, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Request Support");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "");
        c.startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }



    public static void open_url(Context c, String url) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        c.startActivity(i);
    }

    public static String encodeURIComponent(String s) {
        String result;

        try {
            result = URLEncoder.encode(s, "UTF-8")
                    .replaceAll(" ", "%20");
        } catch (UnsupportedEncodingException e) {
            result = s;
        }

        return result;
    }

    public static SharedPreferences notesPrefs;
    private static  final String PREFKRY ="userinfo";
;    public   static SharedPreferences sharedPreferencesToken(Context context){
        notesPrefs = context.getSharedPreferences(PREFKRY, Context.MODE_PRIVATE);
        return notesPrefs;
    }
    public  static Typeface getFont(Context context){
        Locale current = context.getResources().getConfiguration().locale;
        if(current.getLanguage().equals("ar")){
            String fontPath = "fonts/font.TTF";
            return Typeface.createFromAsset(context.getAssets(), fontPath);
        }else{
            String fontPath = "fonts/english.ttf";
            return Typeface.createFromAsset(context.getAssets(), fontPath);
        }

    }
    public static int  product_position ;





    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat, int quality)
    {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(),Base64.NO_WRAP);
    }
    public static Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height,
                matrix, false);

        return resizedBitmap;
    }


    public static String ImageResult(int requestCode , Intent data, ImageView profile, Context context){
        String Image_bitmap_64 = null;
        if (requestCode == REQUEST_CAMERA) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            File destination = new File(Environment.getExternalStorageDirectory(),
                    System.currentTimeMillis() + ".jpg");
            FileOutputStream fo;
            try {
                destination.createNewFile();
                fo = new FileOutputStream(destination);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            profile.setImageBitmap(thumbnail);
            Image_bitmap_64 = encodeToBase64(thumbnail, Bitmap.CompressFormat.JPEG,100);
        } else if (requestCode == SELECT_FILE) {
            Uri selectedImageUri = data.getData();
            String[] projection = {MediaStore.MediaColumns.DATA};
            CursorLoader cursorLoader = new CursorLoader(context, selectedImageUri, projection, null, null,
                    null);
            Cursor cursor = cursorLoader.loadInBackground();
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            cursor.moveToFirst();
            String selectedImagePath = cursor.getString(column_index);
            Bitmap bm;
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(selectedImagePath, options);
            final int REQUIRED_SIZE = 200;
            int scale = 1;
            while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                    && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;
            options.inSampleSize = scale;
            options.inJustDecodeBounds = false;
            bm = BitmapFactory.decodeFile(selectedImagePath, options);
            profile.setImageBitmap(bm);
            Image_bitmap_64 = encodeToBase64(bm, Bitmap.CompressFormat.JPEG,100);
        }

        return  Image_bitmap_64;
    }

    public  static void selectImage(final Context context) {
        final CharSequence[] items = { "Choose from Library", "Cancel" };
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    ((Activity)context).startActivityForResult(intent, REQUEST_CAMERA);

                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    ((Activity)context).startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_FILE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    public  static void setTitleFont(ActionBar actionBar, Context context){
        SpannableString s = new SpannableString(actionBar.getTitle());
        s.setSpan(new com.pointofsale.mivors.helpers.TypefaceSpan(context,context.getString(R.string.standard)), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        actionBar.setTitle(s);
    }

    public static void setLocale(String lang, Context c) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        c.getApplicationContext().getResources().updateConfiguration(config, null);

    }


    public  static int LOCATION_REQUEST = 1000;

   /* public  static void change_language(final Context context) {
        final CharSequence[] items = {  "العربية", "English" };

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        builder.setTitle("Select language");
        final PrefManager prefManager = new PrefManager(context);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("العربية")) {
                    prefManager.update_language("ar");
                    Intent i =  ((Activity) context).getIntent();
                    ((Activity) context).finish();
                    setLocale("ar",context);
                    context.startActivity(i);



                } else if (items[item].equals("English")) {
                    prefManager.update_language("en");
                    Intent i =  ((Activity) context).getIntent();
                    ((Activity) context).finish();
                    setLocale("en", context);
                    context.startActivity(i);

                }
            }
        });
        builder.show();
    }

    public  static void logout(Context context){
        Intent intent = new Intent(context, LoginActivity.class);
        PrefManager prefManager = new PrefManager(context);
        prefManager.logout();
        intent.putExtra("finish", true); // if you are checking for this in your other Activities
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
        ((Activity) context).finish();
    }*/

}
