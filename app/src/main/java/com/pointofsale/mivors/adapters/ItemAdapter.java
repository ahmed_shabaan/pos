package com.pointofsale.mivors.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pointofsale.mivors.R;
import com.pointofsale.mivors.models.item.Item;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Ahmed shaban on 10/3/2017.
 */

public class ItemAdapter   extends RecyclerView.Adapter<ItemAdapter.ViewHolder> {

    private List<Item> dataSet;
    public Context context;


    public ItemAdapter(List<Item> os_versions, Context context) {

        dataSet = os_versions;
        this.context = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemLayoutView = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.product_item, null);
        itemLayoutView.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ItemAdapter.ViewHolder viewHolder,  int i) {

        Item fp = dataSet.get(i);
        viewHolder.price.setText(fp.getPrice());
        viewHolder.quantity.setText(fp.getQuantity()+"");
        viewHolder.name.setText(fp.getName());
        viewHolder.feed = fp;
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.name)
        TextView name;
        @Bind(R.id.price)
        TextView price;
        @Bind(R.id.quantity)
        TextView quantity;
        Item feed;
        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            ButterKnife.bind(this
            ,itemLayoutView);


        }

    }
}
