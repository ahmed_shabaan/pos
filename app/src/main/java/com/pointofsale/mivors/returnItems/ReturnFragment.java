package com.pointofsale.mivors.returnItems;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pointofsale.mivors.R;
import com.pointofsale.mivors.adapters.ItemAdapter;
import com.pointofsale.mivors.models.item.Item;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ReturnFragment extends Fragment {
    @Bind(R.id.list)
    RecyclerView recyclerView;
    ItemAdapter itemAdapter;
    List<Item> items;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view  =  inflater.inflate(R.layout.fragment_return, container, false);
        ButterKnife.bind(this,view);
        items = new ArrayList<>();
        for (int i  =  0 ; i < 20 ; i++){
            Item item
                    = new Item();
            item.setId(i);
            item.setName("shapcy"+i);
            item.setPrice((1+i+50)+"");
            item.setQuantity(i+10);
            items.add(item);
        }

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        itemAdapter = new ItemAdapter(items,getActivity());
        recyclerView.setAdapter(itemAdapter);
        //getServices();

        return  view;
    }



}
