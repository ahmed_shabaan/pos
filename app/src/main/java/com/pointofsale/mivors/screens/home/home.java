package com.pointofsale.mivors.screens.home;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.SubMenu;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.pointofsale.mivors.R;
import com.pointofsale.mivors.helpers.CustomTypefaceSpan;
import com.pointofsale.mivors.helpers.Tokens;
import com.pointofsale.mivors.homeFragment.HomeFragment;
import com.pointofsale.mivors.returnItems.ReturnFragment;
import com.pointofsale.mivors.screens.comission.CommissionFragment;
import com.pointofsale.mivors.screens.createInvoice.createInvoiceStep1.CreateInvoiceStep1Fragment;
import com.pointofsale.mivors.screens.invoiceHistory.InvoicHistoryFragment;
import com.pointofsale.mivors.screens.unavailableItems.UnavailableItemsFragment;

public class home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private FragmentManager manager;
    private FragmentTransaction transaction;
    Fragment fragment
             = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Tokens.setTitleFont(getSupportActionBar(),this);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Menu nav_Menu = navigationView.getMenu();


        for (int i=0;i<nav_Menu.size();i++) {
            MenuItem mi = nav_Menu.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu!=null && subMenu.size() >0 ) {
                for (int j=0; j <subMenu.size();j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }

            //the method we have create in activity
            applyFontToMenuItem(mi);
        }

        fragment = new HomeFragment();
        openFragment(fragment,"home");
    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), getString(R.string.bold));
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("" , font), 0 , mNewTitle.length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.home) {
            fragment = new HomeFragment();
            openFragment(fragment,"home");
        }else if(id == R.id.create_invoice){
            fragment = new CreateInvoiceStep1Fragment();
            openFragment(fragment,"create_invoice_step1");
        } else if (id == R.id.commissions) {
            fragment = new CommissionFragment();
            openFragment(fragment,"CommissionFragment");
        } else if (id == R.id.unavailable_items) {
            fragment = new UnavailableItemsFragment();
            openFragment(fragment,"UnavailableItemsFragment");
        } else if (id == R.id.returns) {
            fragment = new ReturnFragment();
            openFragment(fragment,"ReturnFragment");
        }else if(id == R.id.invoice_history){
            fragment = new InvoicHistoryFragment();
            openFragment(fragment,"InvoiceHistoryFragment");
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void openFragment(Fragment fragment, String tag) {
        manager = getSupportFragmentManager();
        if(manager == null)
            manager  =  getSupportFragmentManager();
        transaction = manager.beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
        if(tag.equals("home")){
            transaction.replace(R.id.content_home, fragment, tag).commit();
        }else{
            Fragment currentFrag = manager.findFragmentById(R.id.content_home);
            if (currentFrag != null && currentFrag.getClass().equals(fragment.getClass())) {

            } else {
                transaction.replace(R.id.content_home, fragment).addToBackStack(null).commit();
            }
        }

    }
}
