package com.pointofsale.mivors.screens.splash;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.pointofsale.mivors.MainActivity;
import com.pointofsale.mivors.R;
import com.pointofsale.mivors.screens.login.LoginActivity;

public class splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        if(getSupportActionBar()!=null){
            getSupportActionBar().hide();
        }

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                try {

                    Intent i;
                    i = new Intent(splash.this, LoginActivity.class);
                    startActivity(i);

                    finish();
                } catch (Exception e) {

                }
            }
        }, 2000);
    }
}
