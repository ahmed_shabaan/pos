package com.pointofsale.mivors.screens.invoiceHistory;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pointofsale.mivors.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class InvoicHistoryFragment extends Fragment {


    public InvoicHistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_invoic_history, container, false);
    }

}
