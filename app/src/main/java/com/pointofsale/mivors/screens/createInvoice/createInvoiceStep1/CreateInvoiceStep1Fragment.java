package com.pointofsale.mivors.screens.createInvoice.createInvoiceStep1;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pointofsale.mivors.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreateInvoiceStep1Fragment extends Fragment {


    public CreateInvoiceStep1Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View  view =  inflater.inflate(R.layout.fragment_create_invoice_step1k, container, false);

        return view;
    }

}
