package com.pointofsale.mivors.screens.comission;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pointofsale.mivors.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class CommissionFragment extends Fragment {


    public CommissionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_commission, container, false);


        return view;
    }

}
